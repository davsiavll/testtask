<?php

namespace Shop\Model\database;

class DBcontroller
{
    //Database connection Properties
    protected $host = 'localhost';
    protected $user = 'root';
    protected $password = '';
    protected $database = 'items_list';

    // connection property
    public $con = null;

    //call constructor
    public function __construct()
    {
        $this -> con = new \mysqli($this->host, $this->user, $this->password, $this->database);
        if ($this->con->connect_error) {
            echo "Fail" . $this->con->connect_error;
        }
    }

    public function __destruct()
    {
        $this->closeConnection();
    }


    //for clising connection
    protected function closeConnection()
    {
        if ($this->con != null) {
            $this->con->close();
            $this->con = null;
        }
    }
}

