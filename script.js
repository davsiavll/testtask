document.addEventListener("DOMContentLoaded", function () {
  if (document.querySelector("#productContainer")) {
    displayItems();
  }
  if (document.querySelector("#delete-product-btn")) {
    document
      .querySelector("#delete-product-btn")
      .addEventListener("click", () => deleteCheckeditems());
  }
  if (document.querySelector("#productType")) {
    document
      .querySelector("#productType")
      .addEventListener("change", () => handleItemChange());
  }
  if (document.querySelector("#product_form")) {
    document
      .querySelector("#productSave")
      .addEventListener("click", (e) => handleProductAdd(e));
  }
});

function deleteCheckeditems() {
  //get all selected items from page
  var items = document.querySelectorAll(".delete-checkbox:checked");
  //compose all selected item sql's into array
  var itemsarray = [];
  items.forEach((item) => {
    itemsarray.push(item.name);
  });
  console.log(JSON.stringify(itemsarray));
  //post array of names
  fetch("./_delete-item.php", {
    method: "POST",
    body: JSON.stringify(itemsarray),
  })
    .then((response) => response.json())
    .then((data) => {
      console.log(data);
      //return to main page after deleting items
      location.assign("./index.php");
    });
}
function handleItemChange() {
  //get all the product tpes from page
  var productTypes = document.querySelector("#productType");
  var i;
  for (i = 0; i < productTypes.length; i++) {
    //disable all elements with #id of produt type
    var element = document.querySelector(`#${productTypes.options[i].text}`);
    element.className = "d-none";
    element.disabled = true;
  }
  //enable one element to begin with
  var element = document.querySelector(`#${productTypes.value}`);
  element.className = "d-block";
  element.disabled = false;
}
function handleProductAdd(e) {
  //prevent form from submiting
  e.preventDefault();
  //get the data from form
  var form = document.querySelector("#product_form");
  //post form data
  fetch("./_add-item.php", {
    method: "POST",
    body: new FormData(form),
  })
    .then((response) => response.json())
    .then((data) => {
      //if data validated and sent to database return to main page
      if (data === "sucess") {
        console.log(data);
        location.assign("index.php");
      } else {
        //if data invalid display message near the invalid field
        //clear all previous alerts
        document.querySelector(".alertplaceholder").innerHTML = "";
        //for each entry in returned errors array append a message near invalid field
        for (const [key, value] of Object.entries(data)) {
          console.log(`${key}: ${value}`);
          var alertPlaceholder = document.getElementById(
            key + "AlertPlaceholder"
          );
          alertPlaceholder.innerHTML = "";
          var wrapper = document.createElement("div");
          wrapper.innerHTML = `<div class="alert alert-primary" role="alert">${value}</div>`;
          alertPlaceholder.append(wrapper);
        }
      }
    });
}
function displayItems() {
  //sort items on page based on their id in database
  function sortChildren(parent, comparator) {
    //replace original array with sorted array
    parent.replaceChildren(...Array.from(parent.children).sort(comparator));
  }
  const main = document.getElementById("productContainer");
  //sort elements in ascending order
  sortChildren(main, (a, b) => +a.id.match(/\d+/) - +b.id.match(/\d+/));
}
