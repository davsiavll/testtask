 <!--start #footer-->
 <footer>
    <hr style = "height:10px" class="color-primary color-primary-bg">
        <div class="strip d-flex justify-content-center">
            <h6 class="font-ubuntu text-black-70 mt-2">Scandiweb Test assigment</h6>
        </div>
</footer>
    <!--!start #footer-->

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj" crossorigin="anonymous"></script>
    <!-- custom js-->
    <script src="script.js"></script>
    <!-- !custom js-->
</body>
</html>