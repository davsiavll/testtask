<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop</title>
    
    <!--Bootstrap cdn-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!--custom css file-->
    <link rel = "stylesheet" href="style.css">

    <?php 
    //require functions.php file
    require('functions.php');
    ?>
</head>
<body>
    <!--start #header-->
        <header id = "header">
            <div class="strip d-flex justify-content-between px-4 py-4 color-secondary-bg">
                <h1 class="font-ubuntu font-size-25 text-black-70 mt-2">Product Add</h1>
                <div class="font-ubuntu font-size-16">
                    <input type="submit" value="Save" form="product_form" class = "button btn btn-primary  mx-3 mt-3" id = "productSave">
                    <a href="index.php" class = "button btn btn-primary  mx-3 mt-3">Cancel</a>
                </div>
            </div>
            <hr style = "height:10px" class="color-primary color-primary-bg">
        </header>
    <!--!start #header-->