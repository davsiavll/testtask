<!--start #main-site-->
<main id = "main-site">
<div class="px-4 py-4">
<form id="product_form" method = "POST" action = "./index.php">
  <div class="form-group row mb-3">
    <label for="sku" class="col-sm-2 col-form-label">SKU</label>
    <div class="col-sm-3">
      <input class="form-control" name="sku" id="sku">
    </div>
    <div id="skuAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
  </div>
  <div class="form-group row mb-3">
    <label for="name" class="col-sm-2 col-form-label">Name</label>
    <div class="col-sm-3">
      <input class="form-control" name="name" id="name">
    </div>
    <div id="nameAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
  </div>
  <div class="form-group row mb-3">
    <label for="price" class="col-sm-2 col-form-label">Price($)</label>
    <div class="col-sm-3">
      <input class="form-control" name="price" id="price">
    </div>
    <div id="priceAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
  </div>
  <div class="form-group row mb-3">
      <legend class="col-form-label col-sm-2">Type switcher</legend>
      <div class="col-sm-3">
          <select class="form-control" name="productType" id="productType">
            <option value="DVD">DVD</option>
            <option value="Furniture">Furniture</option>
            <option selected value="Book">Book</option>
          </select>
      </div>
  </div>
  <fieldset class='d-none' id="DVD">
  <div class="form-group row mb-3">
    <label for="size" class="col-sm-2 col-form-label">Size (MB)</label>
    <div class="col-sm-3">
      <input class="form-control" name="size" id="size">
    </div>
    <div id="sizeAlertPlaceholder" class="col-sm-4"></div>
    <div class="col-sm-9 mt-4">
      <p>Please provide size in MB.</p>
    </div>
  </div>
  </fieldset>
  <fieldset class='d-none' id="Furniture">
  <div class="form-group row mb-3">
    <label for="height" class="col-sm-2 col-form-label">Height (CM)</label>
    <div class="col-sm-3">
      <input class="form-control" name="height" id="height">
    </div>
    <div id="heightAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
  </div>
  <div class="form-group row mb-3">
    <label for="width" class="col-sm-2 col-form-label">Width (CM)</label>
    <div class="col-sm-3">
      <input class="form-control" name="width" id="width">
    </div>
    <div id="widthAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
  </div>
  <div class="form-group row mb-3">
    <label for="length" class="col-sm-2 col-form-label">Lenght (CM)</label>
    <div class="col-sm-3">
      <input class="form-control" name="length" id="length">
    </div>
    <div id="lenghtAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
    <div class="col-sm-9 mt-4">
    <p>Plese provide dimensions in HxWxL format.</p>
    </div>

  </div>
  </fieldset>
  <fieldset class='d-block' id="Book">
  <div class="form-group row mb-3">
    <label for="weight" class="col-sm-2 col-form-label">Weight (KG)</label>
    <div class="col-sm-3">
      <input class="form-control" name="weight" id="weight">
    </div>
    <div id="weightAlertPlaceholder" class="alertplaceholder col-sm-4"></div>
    <div class="col-sm-9 mt-4">
    <p>Please provide weight in KG.</p>
    </div>
  </div>
  </fieldset>
</form>
</div>
</main>
<!--!start #main-site-->