<?php 
    //require functions.php file
    require_once('functions.php');
?>

<!--start #main-site-->
<main id = "main-site">

<div class="container-fluid mt-3 mb-3">
    <div class="row g-2" id="productContainer">
        <?php
        $book->getDisplayData();
        $dvd->getDisplayData();
        $furniture->getDisplayData();
        ?>
    </div>
</div>
</main>
<!--!start #main-site-->
