<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Shop</title>
    
    <!--Bootstrap cdn-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">

    <!--custom css file-->
    <link rel = "stylesheet" href="style.css">

    <?php
    //require functions.php file
    require('functions.php');
    ?>
</head>
<body>
    <!--start #header-->
        <header id = "header">
            <div class="strip d-flex justify-content-between px-4 py-4 color-secondary-bg">
                <h1 class="font-ubuntu font-size-25 text-black-70 mt-2">Product list</h1>
                <div class="font-ubuntu font-size-16">
                    <a href="add-product.php" class = "button btn btn-primary  mx-3 mt-3">ADD</a>
                    <button type="button" class = "button btn btn-primary delete-product-btn mx-3 mt-3" id = "delete-product-btn">MASS DELETE</button>
                </div>
            </div>
            <hr style = "height:10px" class="color-primary color-primary-bg">
        </header>
    <!--!start #header-->
