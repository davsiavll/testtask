<?php

require_once 'database/DBcontroller.php';
require_once 'Model/Book.php';
require_once 'Model/Dvd.php';
require_once 'Model/Furniture.php';

use Shop\Model\database\DBcontroller;
use Shop\Model\{Book, Dvd, Furniture};

$db = new DBcontroller();
$book = new Book($db);
$dvd = new Dvd($db);
$furniture = new Furniture($db);
