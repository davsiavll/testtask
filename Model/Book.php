<?php

namespace Shop\Model;
require_once 'Model/Product.php';

class Book extends Product
{
    //init a property for uniqe data
    protected $weight = null;
    //construct object, checking if provided data is valid and inserting provided data into database
    public function __construct($db, array $data = null)
    {
        parent:: __construct($db, $data);
        if ($this->validateUniqeData($this->data)) {
            if (parent::setCommonData()) {
                $this->setUniqeProp();
            }
        }
    }
    protected function setUniqeProp()
    {
        //if no data were passed to object abort and return null
        if (!$this->data) {
            return null;
        }
        //set object properties according to data passed
        $this->weight = $this->data['weight'];
        //prepare and execute statement to perform
        $stmt = $this->db->con->prepare("INSERT INTO book (SKU, weight) 
        VALUES (?, ?)");
        $stmt->bind_param('sd', $this->sku, $this->weight);
        $result = $stmt->execute();
        //return messege to page if satement executed sucessfuly
        if ($result) {
            echo json_encode('sucess');
        } else {
            echo $this->db->con->error;
        }
    }

    protected function validateUniqeData($data)
    {
        //if no data were passed to object abort and return null
        if (!$data) {
            return null;
        }
        //execute common data validation
        parent::validateCommonData($data);
        // check if weight was provided
        if (empty($data['weight'])) {
            $errorMSG = "Please, submit required data";
            $this->errorsArray['weight'] = $errorMSG;
        } else {
            //checl if weight format is correct in KG i.e it is a number,
            //has only one dcimal point and not more than 3 digits after it
            $weight = explode(".", $data['weight']);
            if (!(is_numeric($data['weight'])) || (sizeof($weight) >= 2 && strlen($weight[1]) > 3 )) {
                $errorMSG = "Please, provide the data of indicated type";
                $this->errorsArray['weight'] = $errorMSG;
            }
        }
        //if no errors were found return true
        if (empty($this->errorsArray)) {
            return true;
        } else {
            //else return associative array to a page
            echo json_encode($this->errorsArray);
        };
    }

    protected function queryUniqeProps()
    {   
        //get all SKUs from database for this product types
        $stmt = $this->db->con->prepare("SELECT SKU
        FROM book");
        $stmt->execute();
        $result = $stmt->get_result();
        $resultArray = array();
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {
            $resultArray[] = $row;
        }
        //get common data for this product type entries
        $commonData = parent::queryCommonData($resultArray);
        $stmt = $this->db->con->prepare("SELECT weight
        FROM book WHERE SKU = ?");
        $resultArray = array();
        //construct array with all properties for this product type
        for($i = 0; $i < sizeof($commonData); $i++) {
            $stmt->bind_param('s', $commonData[$i]['SKU']);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            if ($row){
                $commonData[$i]['weight'] = $row['weight'];
                $resultArray[] = $commonData[$i];
            }
        }
        //return properties array to displaydata function
        return $resultArray;
    }

    public function getDisplayData() 
    {
        return $this->displayData();
    }

    protected function displayData()
    {
        //get product properties from database
        $products = $this->queryUniqeProps();
        if ($products) {
            //construct DOM element for each product
            foreach ($products as $item) {
                echo <<<END
                <div class="col-md-2" id = {$item['item_id']}>
                <div class="card">
                    <div class="product-detail-container">
                        <div class="d-flex flex-column align-items-center">
                            <h6 class="mb-0">SKU: {$item['SKU']} <br/></h6>
                            <h6 class="mb-0">{$item['name']} <br/></h6> <br/>
                            <span class="text-danger font-weight-bold"> {$item['price']}$ <br/></span><br/>
                            <h6 class="mb-0">weight: {$item['weight']} KG</h6><br/>
                        </div>
                            <div class="d-flex justify-content-between align-items-center">
                                <div class="delete-checkbox">  
                                <label for="delete-checkbox">
                                <input type = "checkbox" class="delete-checkbox" style="margin: 4px" name = {$item['SKU']}>
                                Delete item
                                </label> 
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            END;
            }
        }
    }
}