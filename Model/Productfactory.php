<?php

namespace Shop\Model;
require_once 'Model/Book.php';
require_once 'Model/Dvd.php';
require_once 'Model/Furniture.php';


class ProductTypeFactory
{
    public function setProducttype($db, array $data)
    {
        return $this->producttype($db, $data);
    }
    //construct instance of product object depending on data sent from user
    private function producttype($db, array $data)
    {
        if (!$data) return null;
        $type = ucfirst(strtolower($data['productType']));
        $productType = '\\' . __NAMESPACE__ . '\\' . $type;
        return new $productType($db, $data);
 
    }
}