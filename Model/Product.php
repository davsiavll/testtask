<?php

namespace Shop\Model;

abstract class Product
{
    //initialize variables
    protected $sku = null;
    protected $price = null;
    protected $name = null;
    protected $db = null;
    protected $data = null;
    protected $errorsArray = null;
    protected $resultArray = null;
    //construct product object with provided data and a db connection
    public function __construct(database\DBcontroller $db, array $data = null)
    {
        if (!isset($db->con)) {
            return null;
        }
        $this->db = $db;
        $this->data = $data;
    }

    public function getDeleteItem($data)
    {
        return $this->deleteItem($data);
    }

    //delete an item which sku were passed via asynchronous request
    private function deleteItem($data)
    {
        if (!$data) {
            return null;
        }
        //prepare sql statement to delete rows from tables where sku = sku passed from user
        $stmt = $this->db->con->prepare("DELETE a.*, b.*, c.*, d.* 
        FROM product AS a
        LEFT JOIN book AS b
        ON a.SKU = b.SKU
        LEFT JOIN dvddisc AS c
        ON a.SKU = c.SKU
        LEFT JOIN furniture AS d
        ON a.SKU = d.SKU WHERE a.SKU = ?");
        //execute statement for every entry in $data array
        for ($i = 0; $i < sizeof($data); $i++) {
            $stmt->bind_param('s', $data[$i]);
            $result = $stmt->execute();
            if ($result) {
                $bool = true;
            } else {
                echo $this->db->con->error;
            }
        }
        //return confirmation to page
        if ($bool) {
            echo json_encode('deleted');
        }
    }

    //get common data for product type from witch this method is called
    protected function queryCommonData($array)
    {
        $resultArray = array();
        $stmt = $this->db->con->prepare("SELECT item_id, SKU, name, price
        FROM product WHERE SKU = ?");
        foreach($array as $item) {
            $stmt->bind_param('s', $item['SKU']);
            $stmt->execute();
            $result = $stmt->get_result();
            $row = mysqli_fetch_array($result, MYSQLI_ASSOC);
            $resultArray[] = $row;
        }
        //return array of common data to product type method
        return $resultArray;
    }

    //sets data common for all products
    protected function setCommonData()
    {
        //if no data were passed to object abort and return null
        if (!$this->data) {
            return null;
        }
        //set object properties according to data passed
        $this->sku = $this->data['sku'];
        $this->name = $this->data['name'];
        $this->price = $this->data['price'];
        //prepare and execute statement to perform
        $stmt = $this->db->con->prepare("INSERT INTO product (name, price, SKU) 
        VALUES (?, ?, ?)");
        $stmt->bind_param('sds', $this->name, $this->price, $this->sku);
        $result = $stmt->execute();
        //return true if satement executed sucessfuly
        if ($result) {
            return true;
        } else {
            echo $this->db->con->error;
        }
    }

    protected function validateCommonData($data)
    {
        //if no data were passed abort and return null
        if (!$this->data) {
            return null;
        }
        // check if sku was provided
        if (empty($data['sku'])) {
            $errorMSG = "Please, submit required data";
            $this->errorsArray['sku'] = $errorMSG;
        } else {
            //check if sku already in databse
            $sku = $data['sku'];
            $sql = "SELECT COUNT(*) FROM product WHERE SKU = '$sku'";
            $result = $this->db->con->query($sql);
            $row = $result->fetch_row();
            if ($row[0] == 1) {
                $errorMSG = "Please, provide the data of indicated type";
                $this->errorsArray['sku'] = $errorMSG;
            }
        }
        // check if name was provided
        if (empty($data['name'])) {
            $errorMSG = "Please, submit required data";
            $this->errorsArray['name'] = $errorMSG;
        }
        // check if price was provided
        if (empty($data['price'])) {
            $errorMSG = "Please, submit required data";
            $this->errorsArray['price'] = $errorMSG;
        } else {
            //check if data formatted as price
            $digits = explode(".", $data['price']);
            if (!(is_numeric($data['price'])) || (sizeof($digits) >= 2 && strlen($digits[1]) > 2 )) {
                $errorMSG = "Please, provide the data of indicated type";
                $this->errorsArray['price'] = $errorMSG;
            }
        }
    }

    abstract protected function validateUniqeData($data);
    abstract protected function setUniqeProp();
    abstract protected function queryUniqeProps();
    abstract protected function displayData();
}

